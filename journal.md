# Journal

## 2022-09-21

Evaluation after 11 letters. See spec11.pdf.

This is ugly illegible garbage.

Considering the word "minimum", the letter-to-letter spacing is
even, but is _much_ bigger than the counter.

Action: reduce letter-to-letter spacing.

This will also improve the kerning situation in pairs like
**ui** and **um**.

The **o** has much narrower spacing.
The **oo** spacing may be narrower than the counter.

I can't make the **nn** spacing the same as the counter, because
then the word "minimum" would mostly look like a series of **i**
strokes.

Counter in **n** has a horizontal gap of 4.142 (√2×10 - 10).

Average of that gap and 10 is 7.071.
Adding half that (3.536) to either
side of the vertical stroke at the median height.

### Spaced

I set spacing to 3.536 offset from the vertical stroke.
That is a huge improvement.

How to space **r**?
Set metric to same as **n** (which would make it slightly wider)?
Did that. It's _okay_?

Doing same for **o** **v** **w**.

Fine, but the **ro** spacing is an utter disaster.

Did same for **b** **l** **q**.

## Triospace

Now there are only 3 different spacings: **i**, **n**, **m**.


# END
