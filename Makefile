export LANG =

frak.ttf: Makefile control-frak.csv font-fraktur.svg
	ttf8 -control control-frak.csv -dir frak font-fraktur.svg
	f8name -dir frak control-frak.csv
	assfont -dir frak
